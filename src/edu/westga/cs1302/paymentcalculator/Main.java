package edu.westga.cs1302.paymentcalculator;
	
import java.io.IOException;
import java.net.URL;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

/**
 * The main class for the monthly payment calculator application.
 * 
 * @version 11/1/20
 * @author Fernando Dominguez
 * 
 *
 */

public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		primaryStage.setTitle("Lab 7 by Fernando Dominguez");
		try {
			URL theUrl = super.getClass().getResource("view/View.fxml");
			AnchorPane thePane = FXMLLoader.load(theUrl);
			Scene scene = new Scene(thePane);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (IOException exception) {
			exception.printStackTrace();
			Alert message = new Alert(AlertType.ERROR);
			message.setContentText("Problem opeing file");
			message.showAndWait();
		} catch (NullPointerException exception) {
			exception.printStackTrace();
			Alert message = new Alert(AlertType.WARNING);
			message.setContentText("Problem opeing file");
			message.showAndWait();
		} catch (Exception exception) {
			exception.printStackTrace();
		}
	}
	
	/**
	 * main method - the entry point into the application
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @param args not used
	 */
	public static void main(String[] args) {
		launch(args);
	}
}
