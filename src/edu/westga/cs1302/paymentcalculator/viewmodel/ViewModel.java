package edu.westga.cs1302.paymentcalculator.viewmodel;

import java.text.NumberFormat;
import java.util.Locale;

import edu.westga.cs1302.paymentcalculator.model.PaymentCalculator;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/** The viewmodel class.
* 
* @version 11/1/20
* @author Fernando Dominguez
* 
*
*/
public class ViewModel {
	private DoubleProperty initialLoanProperty;
	private DoubleProperty annualRateProperty;
	private IntegerProperty yearsProperty;
	private StringProperty monthlyPaymentProperty;
	
	private PaymentCalculator paymentCalculator;
	
	/** Instantiates the View Model Class.
	* 
	* @precondition none
	* @postcondition creates a new PaymentCalculator
	* 
	*
	*/
	public ViewModel() {
		this.initialLoanProperty = new SimpleDoubleProperty();
		this.annualRateProperty = new SimpleDoubleProperty();
		this.yearsProperty = new SimpleIntegerProperty();
		this.monthlyPaymentProperty = new SimpleStringProperty();
		
		this.paymentCalculator = new PaymentCalculator(0.0, 0, 0.0);
	}
	/** Returns the Initial Loan Amount Property
	* 
	* @precondition none
	* @postcondition none
	* 
	* @return the property for the inital loan amount
	* 
	*
	*/
	
	public DoubleProperty getInitialLoanAmount() {
		return this.initialLoanProperty;
	}
	/** Returns the Annual Interest Rate Property
	* 
	* @precondition none
	* @postcondition none
	* 
	* @return the property for the Annual Interest Rate
	* 
	*
	*/
	
	public DoubleProperty getAnnualInterestRate() {
		return this.annualRateProperty;
	}
	
	/** Returns the Number of Years of the Loan Property
	* 
	* @precondition none
	* @postcondition none
	* 
	* @return the property for the Number Of Years
	* 
	*
	*/
	
	public IntegerProperty getNumberOfYears() {
		return this.yearsProperty;
	}
	/** Returns the Monthly Payment Property
	* 
	* @precondition none
	* @postcondition none
	* 
	* @return the property for the Monthly Payment
	* 
	*
	*/
	
	public StringProperty getMonthlyPayment() {
		return this.monthlyPaymentProperty;
	}
	/** Generates the monthly payment and set the monthly payment to the calculated payment
	* 
	* @precondition none
	* @postcondition none
	* 
	*/
	
	public void calculatePayment() {
		NumberFormat nf = NumberFormat.getCurrencyInstance(Locale.US);
		this.paymentCalculator.setAnnualInterestRate(this.annualRateProperty.getValue());
		this.paymentCalculator.setInitialAmount(this.initialLoanProperty.getValue());
		this.paymentCalculator.setTermInYears(this.yearsProperty.getValue());
		double paymentAmount = this.paymentCalculator.calculateMonthlyPayment();
		String payment = nf.format(paymentAmount);
		
		this.monthlyPaymentProperty.set("The monthly payment is: " + payment);
	}

}
