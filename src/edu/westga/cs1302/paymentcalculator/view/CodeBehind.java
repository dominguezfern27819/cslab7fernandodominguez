package edu.westga.cs1302.paymentcalculator.view;

import edu.westga.cs1302.paymentcalculator.viewmodel.ViewModel;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.util.converter.NumberStringConverter;
/**
 * The CodeBehind class used for the gui file for the payment calculator.
 * 
 * @version 11/1/20
 * @author Fernando Dominguez
 * 
 */

public class CodeBehind {

    @FXML
    private Label lblAmount;

    @FXML
    private TextField txtAmount;

    @FXML
    private Label lblTerm;

    @FXML
    private TextField txtTerm;

    @FXML
    private Label lblRate;

    @FXML
    private TextField txtRate;

    @FXML
    private TextArea txtAreaPayment;
    
    private ViewModel theviewModel;
    
    /**
	 * CodeBehind method to instantiate viewmodel
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 */
    
    public CodeBehind() {
    	this.theviewModel = new ViewModel();
    }
    
    @FXML 
    private void initialize() {
    	this.txtAreaPayment.setEditable(false);
    	
    	this.txtRate.textProperty().bindBidirectional(this.theviewModel.getAnnualInterestRate(), new NumberStringConverter());
    	this.txtAmount.textProperty().bindBidirectional(this.theviewModel.getInitialLoanAmount(), new NumberStringConverter());
    	this.txtTerm.textProperty().bindBidirectional(this.theviewModel.getNumberOfYears(), new NumberStringConverter());
    	this.txtAreaPayment.textProperty().bindBidirectional(this.theviewModel.getMonthlyPayment());
    }
    
    @FXML
    void calculatePayment(ActionEvent event) {
    	this.theviewModel.calculatePayment();
    }
}
