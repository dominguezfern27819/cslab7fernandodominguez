package edu.westga.cs1302.paymentcalculator.model;

/**
 * This class models a monthly loan payment by taking an initial
 *  amount, a number of years for the loan, and the annual interest rate
 * 
 * @author	CS1302
 * @version Fall 2020
 */
public class PaymentCalculator {
	private static final String NEGATIVE_INITIAL_AMOUNT = "the initial amount >= 0";
	private static final String NEGATIVE_NUMBER_OF_YEARS = "the number of years must be >= 0";
	private static final String NEGATIVE_INTEREST_RATE = "the interest rate must be >= 0";

	private double initialAmount;
	private int termInYears;
	private double annualInterestRate;

	/**
	 * Instantiates a new payment calculator.
	 * 
	 * @precondition initialAmount >= 0 AND numberOfYears >= 0 AND
	 *               annualInterestRate >= 0
	 * @postcondition getInitialAmount() == initialAmount AND getTermInYears() ==
	 *                termInYears AND getAnnualInterestRate() ==
	 *                annualInterestRate
	 * 
	 * @param initialAmount			the loan amount
	 * @param termInYears			the loan term in years
	 * @param annualInterestRate	the annual interest rate
	 */
	public PaymentCalculator(double initialAmount, int termInYears, double annualInterestRate) {
		if (initialAmount < 0) {
			throw new IllegalArgumentException(NEGATIVE_INITIAL_AMOUNT);
		}
		
		if (termInYears < 0) {
			throw new IllegalArgumentException(NEGATIVE_NUMBER_OF_YEARS);
		}

		if (annualInterestRate < 0) {
			throw new IllegalArgumentException(NEGATIVE_INTEREST_RATE);
		}
		
		this.initialAmount = initialAmount;
		this.termInYears = termInYears;
		this.annualInterestRate = annualInterestRate;
	}

	/**
	 * Gets the initial amount.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the initialAmount
	 */
	public double getInitialAmount() {
		return this.initialAmount;
	}

	/**
	 * Sets the initial amount.
	 *
	 * @precondition initialAmount >= 0
	 * @postcondition getInitialAmount() == initialAmount
	 * 
	 * @param initialAmount
	 *            the initial amount to set
	 */
	public void setInitialAmount(double initialAmount) {
		if (initialAmount < 0) {
			throw new IllegalArgumentException(NEGATIVE_INITIAL_AMOUNT);
		}
		this.initialAmount = initialAmount;
	}

	/**
	 * Gets the term as a number of years.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the term as a number of years
	 */
	public int getTermInYears() {
		return this.termInYears;
	}

	/**
	 * Sets the loan term in years.
	 *
	 * @precondition term in years >= 0
	 * @postcondition getTermInYears() == termInYears
	 * 
	 * @param termInYears
	 *            the loan term in years
	 */
	public void setTermInYears(int termInYears) {
		if (termInYears < 0) {
			throw new IllegalArgumentException(NEGATIVE_NUMBER_OF_YEARS);
		}
		this.termInYears = termInYears;
	}
	
	/**
	 * Gets the annual interest rate.
	 * 
	 * @precondition none
	 * @postcondition none
	 *
	 * @return the annual interest rate
	 */
	public double getAnnualInterestRate() {
		return this.annualInterestRate;
	}

	/**
	 * Sets the annual interest rate.
	 *
	 * @precondition annualInterestRate >= 0
	 * @postcondition getAnnualInterestRate() == annualInterestRate
	 * 
	 * @param annualInterestRate
	 *            the annual interest rate to set
	 */
	public void setAnnualInterestRate(double annualInterestRate) {
		if (annualInterestRate < 0) {
			throw new IllegalArgumentException(NEGATIVE_INTEREST_RATE);
		}

		this.annualInterestRate = annualInterestRate;
	}

	/**
	 * Calculate monthly payment.
	 * 
	 * @precondition none
	 * @postcondition none
	 * 
	 * @return the loan's monthly payment amount
	 */
	public double calculateMonthlyPayment() {
		double monthlyRate = this.annualInterestRate / 12.0 / 100.0;
		int numberOfMonths = this.termInYears * 12;
		
		double numerator = this.initialAmount * monthlyRate * Math.pow(1 + monthlyRate, numberOfMonths);
		double denominator = Math.pow(1 + monthlyRate, numberOfMonths) - 1;
		return numerator / denominator;
	}

}
